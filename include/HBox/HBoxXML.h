/** Read and write HBox tress to/from XML.

Dependancies:
* HBox
* CSTB::XML (tinyxml2 wrapper)

*/

# include <HBox\HBox.h>
# include <CSTB\XML.h>



/// Reads and writes HBox trees to/from XML.
class HBoxXML
{
public:
	/// Writes the specified box properties to the XML element.
	/// Derive from this to write additional user-defined attributes to the XML element.
	virtual void WriteBox( HBox hBox, XML::HELEMENT hParent );

	/// Writes the specified box (and its descendents) to the specified XML file.
	virtual bool WriteBox( HBox hBox, const wchar_t* fileName );

protected:

	/// Returns the value and unit from the specified attribute.
	/// The unit in the returned structure will be UNDEFINED if the specified attribute is not found.
	/// @example ParseAttr( he, "POS_X" );
	static HBox::AValue ParseAttr( XML::HELEMENT he, const char* attr );
	
	/// Converts the specified attribute value to a string and stores it in the specified element.
	static void WriteAttr( XML::HELEMENT he, const char* attr, HBox::AValue val );

	/// Saves or loads a flag. Returns the value of the flag.
	static bool SerializeFlag( XML::HELEMENT he, const char* flagName, bool value, bool load );

	/// Reads or writes the specified HBox to or from the specified XML::HELEMENT.
	static void SerializeBox( HBox hBox, XML::HELEMENT he, bool load );


};