/** HBox lays out your rectangles.

-Adel Amro (http://code-section.com)
*/


# ifndef CSTB_HBOX_H
# define CSTB_HBOX_H


# include <CSTB\Event.h>



//# define HBOX_NAMESPACE CSTB


# ifdef HBOX_NAMESPACE
namespace HBOX_NAMESPACE
{
# endif


struct HBoxData;
struct HBoxSpring;




class HBox
{
public:

	/// This defines the data type used to store box coordinates and attributes.
	/// For typical GUI layout needs, short is enough and saves space.
	typedef short number;

	HBox( HBoxData* p = 0 ) { m_pBoxData = p; }

	static HBox Create( HBox parent = 0, HBox pos = 0, bool update = false );
	static void Destroy( HBox hBox, bool update = false );

	/// Copy the attributes and flags from rhs.
	void Copy( HBox rhs, bool update = true );

	operator bool () const				{ return m_pBoxData != 0; }
	bool operator == ( const HBox& rhs ){ return m_pBoxData == rhs.m_pBoxData; }


	/// @{
	/// Hierarchy methods
	HBox Parent();
	HBox Next();
	HBox Prev();
	HBox FirstChild();
	HBox LastChild();
	int ChildCount();
	void Relocate( HBox hNewParent, HBox newPos = 0 );
	/// @}


	/// @{
	/// HBox Attributes


	/// HBox attribute enum. Not all layouts support all attributes.
	enum Attrib
	{
		ATTRIB_FIRST = 0,
		POS_X, POS_Y,
		WIDTH, HEIGHT,
		MIN_WIDTH, MIN_HEIGHT,
		MAX_WIDTH, MAX_HEIGHT,
		PADDING_LEFT, PADDING_TOP, PADDING_RIGHT, PADDING_BOTTOM,
		MARGIN_LEFT, MARGIN_TOP, MARGIN_RIGHT, MARGIN_BOTTOM,
		//BORDER_LEFT, BORDER_TOP, BORDER_RIGHT, BORDER_BOTTOM,

		ATTRIB_COUNT
	};



	/// Attribute unit. An attribute is 'not set' if its unit is UNDEFINED.
	enum AttrUnit
	{
		UNDEFINED = 0,
		ABS,
		FLEX,
		PERCENT
	};


	/// An attribute value is a pair of unit and value.
	/// If u is UNDEFINED, the attribute is 'not set'.
	struct AValue
	{
		AttrUnit u; number v;
		AValue( AttrUnit _u = UNDEFINED, number _v = 0 ) { u = _u; v = _v; }
		operator bool() const { return u != UNDEFINED; }
	};

	void	SetAttribute( Attrib a, number val, AttrUnit u = ABS, bool update = true );
	void	SetAttribute( Attrib a, AValue v, bool update = true ) { return SetAttribute( a, v.v, v.u, update ); }
	void	GetAttribute( Attrib a, number& val, AttrUnit& u );
	AValue	GetAttribute( Attrib a ) { AValue v; GetAttribute( a, v.v, v.u ); return v; }

	///@{
	/// Typing shortcuts for common attributes
	void SetMargins( number val, AttrUnit u = ABS, bool update = true )
	{
		SetAttribute( MARGIN_LEFT, val, u, false );
		SetAttribute( MARGIN_TOP, val, u, false );
		SetAttribute( MARGIN_RIGHT, val, u, false );
		SetAttribute( MARGIN_BOTTOM, val, u, update );
	}
	void SetPadding( number val, AttrUnit u = ABS, bool update = true )
	{
		SetAttribute( PADDING_LEFT, val, u, false );
		SetAttribute( PADDING_TOP, val, u, false );
		SetAttribute( PADDING_RIGHT, val, u, false );
		SetAttribute( PADDING_BOTTOM, val, u, update );
	}
	void SetSize( number width, number height, AttrUnit u = ABS, bool update = true )
	{
		SetAttribute( WIDTH, width, u, false );
		SetAttribute( HEIGHT, height, u, update );
	}
	/// @}

	/// @}


	/// @{
	/// Flags

	struct Flags
	{
		bool hidden : 1; /// The box is not updated and has no effect on the layout of its siblings (skipped when the layout is updated).
		bool firstChildIsTemplate : 1; /// Uses the attributes of the first child for all children when calculating child layout.
		bool transparent: 1; /// Used with hit-testing (and can be used with drawing).
		//bool freeze : 1; /// Prevents the box from being modified by its parent.
		//bool freezeChildren : 1; /// Children of the box are not updated when the box is updated.

		Flags() { hidden = firstChildIsTemplate = transparent =/*freeze = freezeChildren =*/ false; }
	};

	Flags GetFlags();
	void SetFlags( Flags f, bool update = true );
	/// @}


	/// @{
	/// Child layout

	enum Layout
	{
		ROW,
		COLUMN,
		STACK,
		//FREE // items whose POS_X and POS_Y attributes are floated.
	};

	Layout	GetLayout();
	void	SetLayout( Layout lo );
	/// @}


	/// @{
	/// The boxes

	struct Box
	{
		number top, left, right, bottom;

		Box() { left = top = right = bottom = 0; }
		number Width() const { return right - left; }
		number Height() const { return bottom - top; }
		bool operator == (Box& rhs) const
		{ return top == rhs.top && left == rhs.left && right == rhs.right && bottom == rhs.bottom; }
		bool operator != (Box& rhs) const { return ! (*this == rhs); }
	};

	Box		ContentBox() { return GetBox( CONTENT_BOX ); }
	Box		BorderBox() { return GetBox( BORDER_BOX ); }
	Box		MarginBox() { return GetBox( MARGIN_BOX ); }

	/// @}


	/// Called when an attribute is changed. Recalculates box and lays out children.
	void	Update();
	

	/// @{
	/// user data
	void*	GetUserData();
	void	SetUserData( void* );
	/// @}


	/// @{
	/// Box events

	enum BoxEvent
	{
		ATTACHED,
		DETACHED,
		UPDATED
	};

	static DECLARE_EVENT( void, HBox hBox, BoxEvent evt ) boxEvent;
	/// @}


protected:

	/// Solves a system of linear springs.
	static number SolveSprings( HBoxSpring* springs, int numSprings, number space );

	enum BoxId
	{
		CONTENT_BOX,
		BORDER_BOX,
		MARGIN_BOX
	};

	Box GetBox( BoxId bid );

	operator HBoxData*() const	{ return m_pBoxData; }
	HBoxData&	Data() { return *m_pBoxData; }
	void		UpdateChildren();
	bool		AttrSpring( Attrib a, number space, HBoxSpring& s );

	void		Row();
	void		Column();
	void		Stack();

	HBoxData*	m_pBoxData;
};


# ifdef HBOX_NAMESPACE
};
# endif



# endif // include guard