# include <HBox\HBox.h>
# include <CSTB\IntrusiveTree.h>
# include <CSTB\Win32Util.h> // TODO: Move MIN, MAX, and other non-windows related macros to a separate file.



# ifdef HBOX_NAMESPACE
namespace HBOX_NAMESPACE
{
# endif



const int kAttribCount = (int)HBox::ATTRIB_COUNT;

typedef char AttrUnitType;



DECLARE_EVENT( void, HBox hBox, HBox::BoxEvent evt ) HBox::boxEvent;



/// Data for each HBox.
struct HBoxData
{
	HBox::number	attribs[ kAttribCount ];
	AttrUnitType	units[ kAttribCount ];
	HBox::Layout	layout;
	HBox::Flags		flags;

	HBox::Box	marginBox;
	HBox::Box	borderBox;
	HBox::Box	contentBox;

	void*		userData;

	CSTB::TreeHook	hook;

	HBoxData()
	{
		CLEAR_ARRAY( attribs );
		CLEAR_ARRAY( units );
		layout = HBox::ROW;
		//layout = HBox::UNIFORM_ROW;
		userData = 0;
	}
};



CSTB::IntrusiveTreeT< HBoxData, offsetof(HBoxData, HBoxData::hook) > boxTree;




// static method
HBox HBox::Create( HBox parent, HBox pos, bool update )
{
	if( !parent )
		DebugPrintf( TEXT("box data size: %d\n"), sizeof( HBoxData ) );

	HBox hBox = boxTree.Add( parent, pos );
	TRIGGER_EVENT( boxEvent, hBox, HBox::ATTACHED );
	if( update && parent )
		parent.Update();
	return hBox;
}




// static method
void HBox::Destroy( HBox hBox, bool update )
{
	if( !hBox ) return;
	TRIGGER_EVENT( boxEvent, hBox, HBox::DETACHED );
	HBox parent = hBox.Parent();
	boxTree.Remove( hBox );
	if( parent && update )
	{
		parent.UpdateChildren();
		TRIGGER_EVENT( boxEvent, parent, HBox::UPDATED );
	}
}



void HBox::Copy( HBox rhs, bool update )
{
	if( !rhs || !m_pBoxData ) return;

	memcpy( Data().attribs, rhs.Data().attribs, sizeof( Data().attribs ) );
	memcpy( Data().units, rhs.Data().units, sizeof( Data().units ) );
	Data().layout = rhs.Data().layout;
	Data().flags = rhs.Data().flags;

	if( update ) Update();
}


HBox HBox::Parent() { return boxTree.Parent( *this ); }
HBox HBox::Next() { return boxTree.Next( *this ); }
HBox HBox::Prev() { return boxTree.Prev( *this ); }
HBox HBox::FirstChild() { return boxTree.FirstChild( *this ); }
HBox HBox::LastChild() { return boxTree.LastChild( *this ); }
int HBox::ChildCount() { return boxTree.ChildCount( *this ); }
void HBox::Relocate( HBox newParent, HBox newPos ) { boxTree.Move( *this, newParent, newPos ); }

void HBox::SetAttribute( Attrib a, number val, AttrUnit u, bool update )
{ Data().attribs[ a ] = val; Data().units[a] = (AttrUnitType)u; if( update ) Update(); }

void HBox::GetAttribute( Attrib a, number& val, AttrUnit& u )
{ val = Data().attribs[ a ]; u = (AttrUnit)Data().units[a]; }

HBox::Flags HBox::GetFlags() { return Data().flags; }
void HBox::SetFlags( Flags f, bool update ) { Data().flags = f; if( update ) Update(); }

HBox::Layout HBox::GetLayout() { return Data().layout; }
void HBox::SetLayout( Layout lo ) { Data().layout = lo; }

void* HBox::GetUserData() { return Data().userData; }
void HBox::SetUserData( void* p ) { Data().userData = p; }


HBox::Box HBox::GetBox( BoxId bid )
{
	number dx = 0, dy = 0;
	HBox hParent = Parent();
	if( hParent )
	{
		Box container = hParent.GetBox( CONTENT_BOX );
		dx = container.left;
		dy = container.top;
	}

	Box b;
	switch( bid )
	{
		case CONTENT_BOX: b = Data().contentBox; break;
		case MARGIN_BOX: b = Data().marginBox; break;
		case BORDER_BOX: b = Data().borderBox; break;
	}
	b.left += dx; b.right += dx;
	b.top += dy; b.bottom += dy;
	return b;
}




void HBox::Update()
{
	if( Parent() )
		Parent().UpdateChildren();
	else
	{
		// Calculate the boxes of a root hbox.
		// For now, the size of the root box is the same as the size of the size attributes.
		Data().borderBox.right = GetAttribute( WIDTH ).v;
		Data().borderBox.bottom = GetAttribute( HEIGHT ).v;
		Data().contentBox = Data().borderBox;
		Data().marginBox = Data().borderBox;
	}
	UpdateChildren();

	TRIGGER_EVENT( boxEvent, *this, HBox::UPDATED );
}




void HBox::UpdateChildren()
{
	//if( GetFlags().freezeChildren ) return;

	switch( GetLayout() )
	{
	case ROW:			Row(); break;
	case COLUMN:		Column(); break;
	default: Stack();
	}
}




typedef struct HBoxSpring
{
	// set max to -1 for undefined.
	// if weight is 0, the item is not flexible. Its size will be equal to min
	HBox::number min, max, output, weight;
	HBoxSpring() { min = 0; max = -1; output = 0; weight = 0; }
} Spring;




HBox::number HBox::SolveSprings( Spring* springs, int numSprings, number space )
{
	if( numSprings == 0 )
		return 0;

	number flexSpace = space;
	number totalWeight = 0;

	// First pass, assign minimum size to all springs and see how much space remains.
	// The output here will be the width of the spring, not the position.
	for( int i=0; i<numSprings; i++ )
	{
		Spring& s = springs[i];
		s.output = s.min;
		space -= s.min;
		totalWeight += s.weight;
		if( s.weight == 0 )
			flexSpace -= s.min;
	}

	if( space <= 0 || totalWeight == 0 )
		return space; // All items are at their minimum size, and there is no more space to distribute.

	
	number f = flexSpace / totalWeight;

	for( int i=0; i<numSprings; i++ )
	{
		Spring& s = springs[i];
		if( 0 == s.weight )
			continue;

		if( s.max < 0 )
			s.max = flexSpace;

		s.output = f * s.weight;
		if( s.output < s.min || s.output > s.max )
		{
			// make static.
			s.output = s.output < s.min ? s.min : s.max;
			flexSpace -= s.output;
			totalWeight -= s.weight;
			s.weight = 0;

			if( totalWeight <= 0 )
				break;//return 0;
			
			f = flexSpace / totalWeight;
			i = -1; // reset loop.
		}
	}
	
	// TODO: Should return remaining available space.
	return flexSpace;
}




// Create a spring from the specified attribute.
bool HBox::AttrSpring( Attrib attr, number space, Spring& s )
{
	s = Spring();
	AValue a = GetAttribute( attr );
	if( a.u == UNDEFINED )
		return false;

	AValue min, max;
	if( attr == WIDTH ) { min = GetAttribute( MIN_WIDTH ); max = GetAttribute( MAX_WIDTH ); }
	if( attr == HEIGHT ) { min = GetAttribute( MIN_HEIGHT ); max = GetAttribute( MAX_HEIGHT ); }

	if( min.u == PERCENT ) { min.v = (number)(min.v / 100.f * space); min.u = ABS; }
	if( max.u == PERCENT ) { max.v = (number)(max.v / 100.f * space); max.u = ABS; }

	if( a.u == PERCENT )
	{
		a.v = (number)(a.v / 100.f * space);
		if( min && a.v < min.v ) a.v = min.v;
		if( max && a.v > max.v ) a.v = max.v;
		a.u = ABS;
	}

	
	if( a.u == ABS )
		s.min = a.v;
	else
	{
		if( min ) s.min = min.v;
		if( max ) s.max = max.v;
		s.weight = a.v;
	}

	return true;
}




void HBox::Row()
{
	if( !FirstChild() ) return;

	Box cb = ContentBox();
	number space = cb.Width();

	Spring springs[ 256 ];
	int numSprings = 0;

	for( HBox hChild = FirstChild(); hChild != 0; hChild = hChild.Next() )
	{
		if( hChild.GetFlags().hidden ) continue;
		HBox ch = GetFlags().firstChildIsTemplate ? FirstChild() : hChild;

		ch.AttrSpring( MARGIN_LEFT, space, springs[ numSprings++ ] );

		// Collapse the left margin spring with the right margin spring of the previous child.
		if( numSprings > 1 )
		{
			Spring& s2 = springs[ --numSprings ];
			Spring& s1 = springs[ numSprings-1 ];
			s1.min = MAX( s1.min, s2.min );
			s1.weight = MAX( s1.weight, s2.weight );
		}

		ch.AttrSpring( PADDING_LEFT, space, springs[ numSprings++ ] );
		ch.AttrSpring( WIDTH, space, springs[ numSprings++ ] );
		ch.AttrSpring( PADDING_RIGHT, space, springs[ numSprings++ ] );
		ch.AttrSpring( MARGIN_RIGHT, space, springs[ numSprings++ ] );
	}


	SolveSprings( springs, numSprings, space );
	int iSpring = 0;
	number x = 0;

	for( HBox hChild = FirstChild(); hChild != 0; hChild = hChild.Next() )
	{
		if( hChild.GetFlags().hidden ) continue;
		hChild.Data().marginBox = cb;
		hChild.Data().borderBox = cb;
		hChild.Data().contentBox = cb;

		hChild.Data().marginBox.left = x;	x += springs[iSpring++].output;
		hChild.Data().borderBox.left = x;	x += springs[iSpring++].output;
		hChild.Data().contentBox.left = x;	x += springs[iSpring++].output;
		hChild.Data().contentBox.right = x; x += springs[iSpring++].output;
		hChild.Data().borderBox.right = x;	x += springs[iSpring++].output;
		hChild.Data().marginBox.right = x;	x -= springs[--iSpring].output;
	}

	// Now do heights.
	for( HBox hChild = FirstChild(); hChild != 0; hChild = hChild.Next() )
	{
		if( hChild.GetFlags().hidden ) continue;
		HBox ch = GetFlags().firstChildIsTemplate ? FirstChild() : hChild;

		iSpring = 0;
		space = cb.Height();
		ch.AttrSpring( MARGIN_TOP, space, springs[ iSpring++ ] );
		ch.AttrSpring( PADDING_TOP, space, springs[ iSpring++ ] );
		ch.AttrSpring( HEIGHT, space, springs[ iSpring++ ] );
		ch.AttrSpring( PADDING_BOTTOM, space, springs[ iSpring++ ] );
		ch.AttrSpring( MARGIN_BOTTOM, space, springs[ iSpring++ ] );

		SolveSprings( springs, iSpring, space );

		iSpring = 0;
		number y = 0;
		hChild.Data().marginBox.top = y;		y += springs[ iSpring++ ].output;
		hChild.Data().borderBox.top = y;		y += springs[ iSpring++ ].output;
		hChild.Data().contentBox.top = y;		y += springs[ iSpring++ ].output;
		hChild.Data().contentBox.bottom = y;	y += springs[ iSpring++ ].output;
		hChild.Data().borderBox.bottom = y;		y += springs[ iSpring++ ].output;
		hChild.Data().marginBox.bottom = y;		y += springs[ iSpring++ ].output;
		hChild.UpdateChildren();
	}
}




void HBox::Column()
{
	if( !FirstChild() ) return;

	Box cb = ContentBox();
	number space = cb.Height();

	Spring springs[ 256 ];
	int numSprings = 0;

	for( HBox hChild = FirstChild(); hChild != 0; hChild = hChild.Next() )
	{
		if( hChild.GetFlags().hidden ) continue;

		HBox ch = GetFlags().firstChildIsTemplate ? FirstChild() : hChild;
		ch.AttrSpring( MARGIN_TOP, space, springs[ numSprings++ ] );

		// Collapse the left margin spring with the right margin spring of the previous child.
		if( numSprings > 1 )
		{
			Spring& s2 = springs[ --numSprings ];
			Spring& s1 = springs[ numSprings-1 ];
			s1.min = MAX( s1.min, s2.min );
			s1.weight = MAX( s1.weight, s2.weight );
		}

		ch.AttrSpring( PADDING_TOP, space, springs[ numSprings++ ] );
		ch.AttrSpring( HEIGHT, space, springs[ numSprings++ ] );
		ch.AttrSpring( PADDING_BOTTOM, space, springs[ numSprings++ ] );
		ch.AttrSpring( MARGIN_BOTTOM, space, springs[ numSprings++ ] );
	}


	SolveSprings( springs, numSprings, space );
	int iSpring = 0;
	number y = 0;

	for( HBox hChild = FirstChild(); hChild != 0; hChild = hChild.Next() )
	{
		if( hChild.GetFlags().hidden ) continue;
		hChild.Data().marginBox = cb;
		hChild.Data().borderBox = cb;
		hChild.Data().contentBox = cb;

		hChild.Data().marginBox.top = y;		y += springs[iSpring++].output;
		hChild.Data().borderBox.top = y;		y += springs[iSpring++].output;
		hChild.Data().contentBox.top = y;		y += springs[iSpring++].output;
		hChild.Data().contentBox.bottom = y;	y += springs[iSpring++].output;
		hChild.Data().borderBox.bottom = y;		y += springs[iSpring++].output;
		hChild.Data().marginBox.bottom = y;		y -= springs[--iSpring].output;
	}

	// Now do widths.
	for( HBox hChild = FirstChild(); hChild != 0; hChild = hChild.Next() )
	{
		if( hChild.GetFlags().hidden ) continue;
		HBox ch = GetFlags().firstChildIsTemplate ? FirstChild() : hChild;
		iSpring = 0;
		space = cb.Width();
		ch.AttrSpring( MARGIN_LEFT, space, springs[ iSpring++ ] );
		ch.AttrSpring( PADDING_LEFT, space, springs[ iSpring++ ] );
		ch.AttrSpring( WIDTH, space, springs[ iSpring++ ] );
		ch.AttrSpring( PADDING_RIGHT, space, springs[ iSpring++ ] );
		ch.AttrSpring( MARGIN_RIGHT, space, springs[ iSpring++ ] );

		SolveSprings( springs, iSpring, space );

		iSpring = 0;
		number x = 0;
		hChild.Data().marginBox.left = x;		x += springs[ iSpring++ ].output;
		hChild.Data().borderBox.left = x;		x += springs[ iSpring++ ].output;
		hChild.Data().contentBox.left = x;		x += springs[ iSpring++ ].output;
		hChild.Data().contentBox.right = x;		x += springs[ iSpring++ ].output;
		hChild.Data().borderBox.right = x;		x += springs[ iSpring++ ].output;
		hChild.Data().marginBox.right = x;		x += springs[ iSpring++ ].output;

		hChild.UpdateChildren();
	}
}




void HBox::Stack()
{
	if( !FirstChild() ) return;

	Box cb = ContentBox();

	Spring springs[ 8 ];
	int numSprings = 0;
	int iSpring = 0;

	for( HBox hChild = FirstChild(); hChild != 0; hChild = hChild.Next() )
	{
		if( hChild.GetFlags().hidden ) continue;
		// Calculate horizontally
		iSpring = 0;
		number space = cb.Width();
		hChild.AttrSpring( MARGIN_LEFT, space, springs[ iSpring++ ] );
		hChild.AttrSpring( PADDING_LEFT, space, springs[ iSpring++ ] );
		hChild.AttrSpring( WIDTH, space, springs[ iSpring++ ] );
		hChild.AttrSpring( PADDING_RIGHT, space, springs[ iSpring++ ] );
		hChild.AttrSpring( MARGIN_RIGHT, space, springs[ iSpring++ ] );

		SolveSprings( springs, iSpring, space );

		iSpring = 0;
		number x = 0;
		hChild.Data().marginBox.left = x;		x += springs[ iSpring++ ].output;
		hChild.Data().borderBox.left = x;		x += springs[ iSpring++ ].output;
		hChild.Data().contentBox.left = x;		x += springs[ iSpring++ ].output;
		hChild.Data().contentBox.right = x;		x += springs[ iSpring++ ].output;
		hChild.Data().borderBox.right = x;		x += springs[ iSpring++ ].output;
		hChild.Data().marginBox.right = x;		x += springs[ iSpring++ ].output;


		// Calculate vertically
		iSpring = 0;
		space = cb.Height();
		hChild.AttrSpring( MARGIN_TOP, space, springs[ iSpring++ ] );
		hChild.AttrSpring( PADDING_TOP, space, springs[ iSpring++ ] );
		hChild.AttrSpring( HEIGHT, space, springs[ iSpring++ ] );
		hChild.AttrSpring( PADDING_BOTTOM, space, springs[ iSpring++ ] );
		hChild.AttrSpring( MARGIN_BOTTOM, space, springs[ iSpring++ ] );

		SolveSprings( springs, iSpring, space );

		iSpring = 0;
		number y = 0;
		hChild.Data().marginBox.top = y;		y += springs[ iSpring++ ].output;
		hChild.Data().borderBox.top = y;		y += springs[ iSpring++ ].output;
		hChild.Data().contentBox.top = y;		y += springs[ iSpring++ ].output;
		hChild.Data().contentBox.bottom = y;	y += springs[ iSpring++ ].output;
		hChild.Data().borderBox.bottom = y;		y += springs[ iSpring++ ].output;
		hChild.Data().marginBox.bottom = y;		y += springs[ iSpring++ ].output;

		hChild.UpdateChildren();
	}
}

# ifdef HBOX_NAMESPACE
};
# endif
