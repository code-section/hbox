# include <HBox\HBoxXML.h>
# include <CSTB\Win32Util.h>



HBox::AValue HBoxXML::ParseAttr( XML::HELEMENT he, const char* attr )
{
	HBox::AValue v;
	const char* sAttr = he.GetAttribute( attr );
	if( !sAttr ) return v;

	char u = 0;
	if( sscanf_s( sAttr, "%d%c", &v.v, &u ) < 1 )
		return v;
	v.u = HBox::ABS;
	if( u == '%' ) v.u = HBox::PERCENT;
	if( u == '*' ) v.u = HBox::FLEX;
	return v;
}




void HBoxXML::WriteAttr( XML::HELEMENT he, const char* attr, HBox::AValue v )
{
	if( v.u == HBox::UNDEFINED ) return;

	char attrText[ 8 ]; attrText[0] = 0;
	char u = 0;
	if( v.u == HBox::PERCENT ) u = '%';
	if( v.u == HBox::FLEX ) u = '*';
	sprintf_s( attrText, sizeof(attrText), "%d%c", v.v, u );
	he.SetAttribute( attr, attrText );
}




bool HBoxXML::SerializeFlag( XML::HELEMENT he, const char* flagName, bool value, bool load )
{
	if( load ) return he.GetBoolAttr( flagName, value );
	if( value ) he.SetAttribute( flagName, value );
	return value;
}




void HBoxXML::SerializeBox( HBox hBox, XML::HELEMENT he, bool load )
{
	struct SavedAttr { HBox::Attrib attr; const char* name; };
	SavedAttr a[] = {
		{HBox::POS_X, "POS_X"},
		{HBox::POS_Y, "POS_Y"},
		{HBox::WIDTH, "WIDTH"},
		{HBox::HEIGHT, "HEIGHT"},
		{HBox::MIN_WIDTH, "MIN_WIDTH"},
		{HBox::MIN_HEIGHT, "MIN_HEIGHT"},
		{HBox::MAX_WIDTH, "MAX_WIDTH"},
		{HBox::MAX_HEIGHT, "MAX_HEIGHT"},
		{HBox::PADDING_LEFT, "PADDING_LEFT"},
		{HBox::PADDING_TOP, "PADDING_TOP"},
		{HBox::PADDING_RIGHT, "PADDING_RIGHT"},
		{HBox::PADDING_BOTTOM, "PADDING_BOTTOM"},
		{HBox::MARGIN_LEFT, "MARGIN_LEFT"},
		{HBox::MARGIN_TOP, "MARGIN_TOP"},
		{HBox::MARGIN_RIGHT, "MARGIN_RIGHT"},
		{HBox::MARGIN_BOTTOM, "MARGIN_BOTTOM"},
	};

	HBox::AValue v;
	for( unsigned i=0; i<ARRAY_SIZE(a); i++ )
	{
		if( load ) hBox.SetAttribute( a[i].attr, ParseAttr( he, a[i].name ) );
		else WriteAttr( he, a[i].name, hBox.GetAttribute( a[i].attr ) );
	}

		
	// Serialize flags
		
	HBox::Flags flags = hBox.GetFlags();
		
	flags.hidden = SerializeFlag( he, "hidden", flags.hidden, load );
	flags.transparent = SerializeFlag( he, "transparent", flags.transparent, load );
	flags.firstChildIsTemplate = SerializeFlag( he, "FCIT", flags.firstChildIsTemplate, load );
}




void HBoxXML::WriteBox( HBox hBox, XML::HELEMENT hParent )
{
	XML::HELEMENT he = hParent.CreateChildElement( "box" );
	SerializeBox( hBox, he, false );
	HBox hChild = hBox.FirstChild();
	while( hChild )
	{
		WriteBox( hChild, he );
		hChild = hChild.Next();
	}
}



bool HBoxXML::WriteBox( HBox hBox, const wchar_t* sFile )
{

	XML::Document doc;
	XML::HELEMENT he = doc.CreateElement( "boxtree" );
	if( hBox )
		WriteBox( hBox, he );
	return doc.Save( sFile );
}