/** HBox Preview window. Window which displays an HBox tree.

Dependancies:
	* CSTB
	* Win32

*/

# ifndef HBOX_PREVIEW_WND
# define HBOX_PREVIEW_WND


# include <CSTB\Window.h>
# include <CSTB\Win32Util.h>




class HBoxPreviewWindow : public CSTB::Window
{
public:
	HBox m_hbRoot;
	HBox m_hbSelected;


	HBoxPreviewWindow() { m_hbRoot = m_hbSelected = 0; }


	void DrawBox( HBox hBox, HDC hdc )
	{
		COLORREF color = (COLORREF)hBox.GetUserData();

		if( hBox.GetFlags().hidden )
			return;

		static HPEN hPen = 0;
		static HPEN hSelPen = 0;
		static HBRUSH hBrush = 0;
		static FillRectAlpha rectFiller;

		if( hPen == 0 )
		{
			hPen = CreatePen( PS_SOLID, 1, RGB(255,255,255) );
			hSelPen = CreatePen( PS_DASHDOT, 1, RGB( 150, 150, 255 ) );
			hBrush = CreateHatchBrush( HS_CROSS, RGB(200, 200, 200) );
		}

	
		RECT r;

		// Fill and outline the border box.
		HBox::Box bxBorder = hBox.BorderBox();
		r.left = bxBorder.left;
		r.top = bxBorder.top;
		r.right = bxBorder.right;
		r.bottom = bxBorder.bottom;
		if( !hBox.Parent() )
		{
			HGDIOBJ oldBrush = SelectObject( hdc, hBrush );
			Rectangle( hdc, r.left, r.top, r.right, r.bottom );
			SelectObject( hdc, oldBrush );
		}
		else
		{
			BYTE alpha = 160;
			if( hBox.GetFlags().transparent )
				alpha -= 100;
			rectFiller.FillRect( hdc, r, color, alpha );
		}
		OutlineRect( hdc, r, hPen );


		// outline the content box
		HBox::Box bxContent = hBox.ContentBox();
		if( bxContent != bxBorder )
		{
			r.left = bxContent.left;
			r.top = bxContent.top;
			r.right = bxContent.right;
			r.bottom = bxContent.bottom;
			OutlineRect( hdc, r, hPen );
		}
		OutlineRect( hdc, r, hPen );


		for( HBox hChild = hBox.FirstChild(); hChild != 0; hChild = hChild.Next() )
			DrawBox( hChild, hdc );

		if( hBox == m_hbSelected )
			OutlineRect( hdc, r, hSelPen );
	}




	VOID OnSize()
	{
		if( m_hbRoot )
		{
			RECT rClient;
			GetClientRect( hWnd, &rClient );
			m_hbRoot.SetAttribute( HBox::WIDTH, (HBox::number)RectWidth(rClient), HBox::ABS, false );
			m_hbRoot.SetAttribute( HBox::HEIGHT, (HBox::number)RectHeight(rClient), HBox::ABS, false );
			m_hbRoot.Update();
			Update();
		}
	}




	void Update() { InvalidateRect( hWnd, NULL, 0 ); UpdateWindow( hWnd ); }



	virtual LRESULT MsgProc( UINT Msg, WPARAM wParam, LPARAM lParam )
	{
		switch( Msg )
		{
		case WM_PAINT:
			if( m_hbRoot )
			{
				RECT rClient;
				GetClientRect( hWnd, &rClient );
				PAINTSTRUCT ps = {0};
				HDC hdc = BeginPaint( hWnd, &ps );
				FillRect( hdc, &rClient, RGB(50, 150,50) );
				DrawBox( m_hbRoot, hdc );
				EndPaint( hWnd, &ps );
			}
			return 0;
		}
		return CSTB::Window::MsgProc( Msg, wParam, lParam );
	}
};

# endif // include guard.