# include <CSTB\Window.h>
# include <CSTB\Win32Util.h>
# include <CSTB\SciterWindow.h>
# include <HBox\HBox.h>
# include "HBoxPreviewWindow.h"



HBoxPreviewWindow previewWnd;



void InitScript();




void OnHBoxEvent( HBox hBox, HBox::BoxEvent evt )
{
	if( evt == HBox::ATTACHED )
		hBox.SetUserData( (void*)RGB( rand()%256, rand()%256, rand()%256 ) );
	else if( evt == HBox::UPDATED && previewWnd.hWnd )
		previewWnd.Update(); // TODO: Find a way to remove unnecessary updates.
}





INT WINAPI WinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, INT nShowCmd )
{
	srand( GetTickCount() );
	InitScript();


	LPCTSTR sLayoutDir = TEXT("wlayout");


	previewWnd.QuitOnClose( TRUE );
	previewWnd.Create( 0, CSTB_WINDOWCLASS, TEXT("HBox Demo - code-section.com"),
		WS_VISIBLE | WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,
		NULL, NULL, hInstance, 0 );



	HBox::boxEvent += OnHBoxEvent;

	// Create a few boxes.
	HBox hbRoot = HBox::Create();
	HBox hbMain = HBox::Create( hbRoot );
	hbMain.SetSize( 1, 1, HBox::FLEX, false );
	hbMain.SetMargins( 12, HBox::ABS, false );
	hbMain.SetPadding( 4, HBox::ABS, false );

	HBox hb1 = HBox::Create( hbMain );
	HBox hb2 = HBox::Create( hbMain );
	HBox hb3 = HBox::Create( hbMain );

	hb1.SetAttribute( HBox::WIDTH, 100, HBox::ABS, false );
	hb1.SetAttribute( HBox::HEIGHT, 1, HBox::FLEX, false );
	hb2.SetAttribute( HBox::WIDTH, 1, HBox::FLEX, false );
	hb2.SetAttribute( HBox::HEIGHT, 1, HBox::FLEX, false );
	hb3.SetAttribute( HBox::WIDTH, 100, HBox::ABS, false );
	hb3.SetAttribute( HBox::HEIGHT, 1, HBox::FLEX, false );

	hbRoot.Update();
	previewWnd.m_hbRoot = hbRoot;



	CSTB::SciterWindow controlsWnd;
	controlsWnd.Create( 0, TEXT("controls"),
						WS_VISIBLE | WS_POPUP | WS_CAPTION | WS_THICKFRAME,
						CW_USEDEFAULT, CW_USEDEFAULT, 300, 500,
						previewWnd, 0,
						hInstance, 0 );
	controlsWnd.LoadFile( TEXT("ui.htm") );


	LoadWindowPlacement( sLayoutDir, controlsWnd );
	LoadWindowPlacement( sLayoutDir, previewWnd );


	while( CSTB::Window::CheckMessages( true ) )
		;

	return 0;
}