# include <CSTB\SciterWindow.h>
# include <HBox\HBox.h>
# include <HBox\HBoxXML.h>
# include "HBoxPreviewWindow.h"



extern HBoxPreviewWindow previewWnd;
HBoxXML boxXML;


namespace sciter
{


/// Converts from a box handle to a string for use in tiscript
value Conv( HBox hBox )
{
	if( !hBox ) return TEXT("");
	WCHAR sRet[ 16 ];
	swprintf_s( sRet, 16, TEXT("box-%p"), hBox );
	return sRet;
}


/// Convers from a string (from script) to a box handle.
HBox Conv( const string& s )
{
	HBox hBox = 0;
	swscanf_s( s.c_str(), TEXT("box-%p"), &hBox );
	return hBox;
}




namespace BoxApi
{

/// Returns the root box assigned to the preview window.
value GetRoot() { return Conv( previewWnd.m_hbRoot ); }


/// Creates a new box.
value Create( string parent, string pos )
{ return Conv( HBox::Create( Conv( parent ), Conv(pos), true ) ); }


/// Copies the properties of one box to another box.
value Copy( string src, string dest )
{
	HBox hSrc = Conv(src); if( !hSrc ) return false;
	HBox hDest = Conv(dest); if( !hDest ) return false;
	hDest.Copy( hSrc, true );
	return true;
}


/// Delete the specified box
static bool Delete( string sBox )
{
	HBox hBox = Conv(sBox);
	if( !hBox ) return false;
	HBox::Destroy( hBox, true );
	return true;
}


/// Returns the parent box
static value Parent( string sBox )
{
	HBox hBox = Conv(sBox);
	if( hBox )
		return Conv( hBox.Parent() );
	return value();
}


/// Next sibling box
static value Next( string sBox ) { if( sBox.empty() ) return value(); return Conv( Conv(sBox).Next() ); }


/// Previous sibling box
static value Prev( string sBox ) { if( sBox.empty() ) return value(); return Conv( Conv(sBox).Prev() ); }


/// Returns an array of all immediate children of the specified box
static value Children( string sBox )
{
	HBox hBox = Conv( sBox );
	if( !hBox )
		return value();
	value v;
	for( HBox h = hBox.FirstChild(); h != 0; h = h.Next() )
		v.append( Conv( h ) );
	return v;
}


/// Set the box layout
static value SetLayout( string sBox, int layout )
{
	HBox hBox = Conv( sBox );
	if( !hBox ) return value( false );
	hBox.SetLayout( (HBox::Layout)layout );
	hBox.Update();
	return value(true);
}


/// Returns the box layout
static value GetLayout( string sBox )
{
	HBox hBox = Conv( sBox );
	if( !hBox ) return value();
	return value( (int)hBox.GetLayout() );
}


/// Relocates a box in the tree
/// TODO: Ensure a box is not placed under itself or its descendants
static bool Relocate( string sBox, string newParent, string pos )
{ Conv( sBox ).Relocate( Conv(newParent), Conv(pos) ); return true; }


/// Sets an attribute of the specified box
static bool SetAttribute( string sBox, int attrib, int val, int unit )
{
	HBox hBox = Conv( sBox );
	if( !hBox )
		return false;
	hBox.SetAttribute( (HBox::Attrib)attrib, val, (HBox::AttrUnit)unit, true );
	return true;
}


/// Returns a box attribute
static value GetAttribute( string sBox, int attrib )
{
	HBox hBox = Conv( sBox );
	if( !hBox ) return value();

	HBox::number v = 0;
	HBox::AttrUnit u = HBox::ABS;
	hBox.GetAttribute( (HBox::Attrib)attrib, v, u );

	value ret;
	ret.set_item( "value", v );
	ret.set_item( "unit", (int)u );
	return ret;
}


/// Returns an object representing the flags of the box.
static value GetFlags( string sBox )
{
	HBox hBox = Conv( sBox );
	if( !hBox ) return value();
	value ret;
	ret.set_item( "hidden", hBox.GetFlags().hidden );
	ret.set_item( "transparent", hBox.GetFlags().transparent );
	ret.set_item( "firstChildIsTemplate", hBox.GetFlags().firstChildIsTemplate );
	//ret.set_item( "freeze", hBox.GetFlags().freeze );
	//ret.set_item( "freezeChildren", hBox.GetFlags().freezeChildren );
	return ret;
}


/// Updates the flags of a box.
static bool SetFlags( string sBox, value flags )
{
	HBox hBox = Conv( sBox );
	if( !hBox )
		return false;
	HBox::Flags f = hBox.GetFlags();
	f.hidden = flags.get_item( "hidden" ).get( f.hidden );
	f.transparent = flags.get_item( "transparent" ).get( f.transparent );
	f.firstChildIsTemplate = flags.get_item( "firstChildIsTemplate" ).get( f.firstChildIsTemplate );
	//f.freeze = flags.get_item( "freeze" ).get( f.freeze );
	//f.freezeChildren = flags.get_item( "freezeChildren" ).get( f.freezeChildren );
	hBox.SetFlags( f );
	return true;
}


/// Update the selected box in the preview window.
static bool SetSelectedBox( string sBox )
{
	previewWnd.m_hbSelected = Conv( sBox );
	previewWnd.Update();
	return true;
}


/// Update the preview window.
static bool Update() { previewWnd.m_hbRoot.Update(); return true; }


/// Saves the specifeid box (and its descendants) to the specified file.
static bool SaveBox( string sBox, string sFile )
{
	HBox hBox = Conv( sBox );
	if( !hBox ) return false;
	return boxXML.WriteBox( hBox, sFile.c_str() );
}


/// Returns an object that represents the box API presented to the script engine.
static bool GetApi( HWND hWnd, HELEMENT he, LPCSTR name, UINT argc, SCITER_VALUE* argv, SCITER_VALUE& retval )
{
	value api;
	api.set_item( "Root", vfunc( GetRoot ) );
	api.set_item( "Create", vfunc( Create ) );
	api.set_item( "Delete", vfunc( Delete ) );
	api.set_item( "Copy", vfunc( Copy ) );
	api.set_item( "Parent", vfunc( Parent ) );
	api.set_item( "Next", vfunc( Next ) );
	api.set_item( "Prev", vfunc( Prev ) );
	api.set_item( "Children", vfunc( Children ) );
	api.set_item( "Relocate", vfunc( Relocate ) );
	api.set_item( "GetAttribute", vfunc( GetAttribute ) );
	api.set_item( "SetAttribute", vfunc( SetAttribute ) );
	api.set_item( "GetFlags", vfunc( GetFlags ) );
	api.set_item( "SetFlags", vfunc( SetFlags ) );
	api.set_item( "SetSelectedBox", vfunc( SetSelectedBox ) );
	api.set_item( "SetLayout", vfunc( SetLayout ) );
	api.set_item( "GetLayout", vfunc( GetLayout ) );
	api.set_item( "Update", vfunc( Update ) );
	api.set_item( "SaveBox", vfunc( SaveBox ) );

	// attributes
	api.set_item( "POS_X", HBox::POS_X );
	api.set_item( "POS_Y", HBox::POS_Y );
	api.set_item( "WIDTH", HBox::WIDTH );
	api.set_item( "HEIGHT", HBox::HEIGHT );
	api.set_item( "MIN_WIDTH", HBox::MIN_WIDTH );
	api.set_item( "MIN_HEIGHT", HBox::MIN_HEIGHT );
	api.set_item( "MAX_WIDTH", HBox::MAX_WIDTH );
	api.set_item( "MAX_HEIGHT", HBox::MAX_HEIGHT );
	api.set_item( "PADDING_LEFT", HBox::PADDING_LEFT );
	api.set_item( "PADDING_TOP", HBox::PADDING_TOP );
	api.set_item( "PADDING_RIGHT", HBox::PADDING_RIGHT );
	api.set_item( "PADDING_BOTTOM", HBox::PADDING_BOTTOM );
	api.set_item( "MARGIN_LEFT", HBox::MARGIN_LEFT );
	api.set_item( "MARGIN_TOP", HBox::MARGIN_TOP );
	api.set_item( "MARGIN_RIGHT", HBox::MARGIN_RIGHT );
	api.set_item( "MARGIN_BOTTOM", HBox::MARGIN_BOTTOM );

	// units
	api.set_item( "UNDEFINED", HBox::UNDEFINED );
	api.set_item( "ABS", HBox::ABS );
	api.set_item( "FLEX", HBox::FLEX );
	api.set_item( "PERCENT", HBox::PERCENT );

	// box id
	/*
	api.set_item( "CONTENT_BOX", HBox::CONTENT_BOX );
	api.set_item( "BORDER_BOX", HBox::BORDER_BOX );
	api.set_item( "MARGIN_BOX", HBox::MARGIN_BOX );
	*/

	// layout
	api.set_item( "ROW", HBox::ROW );
	api.set_item( "COLUMN", HBox::COLUMN );
	api.set_item( "STACK", HBox::STACK );

	retval = api;
	return true;
}

};



VOID SC_CALLBACK DebugOutput( LPVOID param, UINT subsystem, UINT severity, LPCWSTR sText, UINT len )
{
	OutputDebugString( TEXT("sciter msg: " ) );
	OutputDebugString( sText );
}


};


void InitScript()
{
	SciterSetupDebugOutput( 0, 0, sciter::DebugOutput );
	CSTB::SciterWindow::RegisterScriptFunction( sciter::BoxApi::GetApi, "BoxApi" );
}